x = 42
file = open("data.txt", "r")

while True:
    y = file.readline()
    if not y:
        break

    x += 1 / int(y)
file.close()
print(x)
