import time

array = []

file = open('earth.md', 'r')
string = ''.join([line for line in file])
frameNum = 1

while frameNum < 26:
    frame = string.split(f'\n```\n')
    array.append(str(frame[frameNum]))
    frameNum += 1

for i in range(0, len(array)):
        if i % 2 == 0:
            print(f'\033[96m{array[i]}\033[0m')
            time.sleep(1)
            print('\n' * 100)
file.close()
